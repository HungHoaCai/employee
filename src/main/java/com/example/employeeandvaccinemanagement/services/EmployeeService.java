package com.example.employeeandvaccinemanagement.services;

import com.example.employeeandvaccinemanagement.entities.Employee;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    List<Employee> findAll();

    Optional<Employee> findById(Integer id);

    Employee save(Employee employee);

    void delete(Integer id);
}
