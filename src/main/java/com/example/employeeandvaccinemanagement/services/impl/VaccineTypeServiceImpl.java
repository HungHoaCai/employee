package com.example.employeeandvaccinemanagement.services.impl;

import com.example.employeeandvaccinemanagement.entities.VaccineType;
import com.example.employeeandvaccinemanagement.repositorys.VaccineTypeRepository;
import com.example.employeeandvaccinemanagement.services.VaccineTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class VaccineTypeServiceImpl implements VaccineTypeService {
    @Autowired
    private VaccineTypeRepository vaccineTypeRepository;
    @Override
    public List<VaccineType> fillAll() {
        return vaccineTypeRepository.findAll();
    }

    @Override
    public Optional<VaccineType> findById(String id) {
        return vaccineTypeRepository.findById(id);
    }

    @Override
    public VaccineType save(VaccineType vaccineType) {
        return vaccineTypeRepository.save(vaccineType);
    }

    @Override
    public void delete(String id) {
         vaccineTypeRepository.deleteById(id);
    }
}
