package com.example.employeeandvaccinemanagement.services.impl;

import com.example.employeeandvaccinemanagement.entities.Employee;
import com.example.employeeandvaccinemanagement.repositorys.EmployeeRepository;
import com.example.employeeandvaccinemanagement.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    @Override
    public Optional<Employee> findById(Integer id) {
        return employeeRepository.findById(id);
    }

    @Override
    public Employee save(Employee employee) {
        return employeeRepository.save(employee);
    }

    @Override
    public void delete(Integer id) {
         employeeRepository.deleteById(id);
    }

}
