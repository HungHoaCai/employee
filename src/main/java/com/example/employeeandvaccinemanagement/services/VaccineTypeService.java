package com.example.employeeandvaccinemanagement.services;


import com.example.employeeandvaccinemanagement.entities.VaccineType;

import java.util.List;
import java.util.Optional;

public interface VaccineTypeService {
    List<VaccineType> fillAll();

    Optional<VaccineType> findById(String id);

    VaccineType save(VaccineType vaccineType);

    void delete(String id);
}
