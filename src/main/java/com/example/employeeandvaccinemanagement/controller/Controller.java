package com.example.employeeandvaccinemanagement.controller;

import com.example.employeeandvaccinemanagement.entities.Employee;
import com.example.employeeandvaccinemanagement.entities.VaccineType;
import com.example.employeeandvaccinemanagement.services.EmployeeService;
import com.example.employeeandvaccinemanagement.services.VaccineTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@org.springframework.stereotype.Controller
public class Controller {
    public static final String currentDirectory = System.getProperty("user.dir");
    public static final Path PATH = Paths.get(currentDirectory + Paths.get("/target/classes/static/image"));
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private VaccineTypeService vaccineTypeService;
   @GetMapping(value = "employee")
    public  String getAllEmployee(Model model){
       model.addAttribute("listEmployee", employeeService.findAll());
       return"main";
   }
   @GetMapping(value = "employee/add")
    public String gerFormAddEmployee(Model model){
       model.addAttribute("emp", new Employee());
     return "addform";
   }
   @PostMapping(value = "employee/add")
   public String postFormAddEmployee(@ModelAttribute("emp") Employee employee, @RequestParam MultipartFile photo){
       if(!photo.isEmpty()){
           try {
               InputStream stream= photo.getInputStream();
               Files.copy(stream, PATH.resolve(photo.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
               employee.setImage(photo.getOriginalFilename());
           }catch (IOException e){
               e.printStackTrace();
           }
       }

       employeeService.save(employee);
       return "redirect:/employee";
   }
   @GetMapping(value = "employee/update/{id}")
   public String UpdateFormAddEmployee(Model model, @PathVariable Integer id){
       model.addAttribute("emp", employeeService.findById(id).get());
       return "addform";
   }
    @GetMapping(value = "employee/delete/{id}")
    public String DealteFormAddEmployee(Model model, @PathVariable Integer id){
       employeeService.delete(id);
        return "redirect:/employee";
    }
///////////////////////////////////////////////////////////
    /* PHẦN VACCINETYPE*/

    /*

     */

    @GetMapping(value = "vaccineType")
    public  String getAllVaccineType(Model model){
        model.addAttribute("listVaccineType", vaccineTypeService.fillAll());
        return"VaccineTypeMain";
    }
    @GetMapping(value = "vaccineType/add")
    public String getFormAddVaccineType(Model model){
        model.addAttribute("vaccineType", new VaccineType());
        return "vaccineTypeAdd";
    }

    @PostMapping(value = "vaccineType/add")
    public String postFormAddVaccineType(@ModelAttribute("vaccineType") VaccineType vaccineType, @RequestParam MultipartFile photo){
        if(!photo.isEmpty()){
            try {
                InputStream stream= photo.getInputStream();
                Files.copy(stream, PATH.resolve(photo.getOriginalFilename()), StandardCopyOption.REPLACE_EXISTING);
                vaccineType.setImage(photo.getOriginalFilename());
            }catch (IOException e){
                e.printStackTrace();
            }
        }

        vaccineTypeService.save(vaccineType);
        return "redirect:/VaccineTypeMain";
    }
    @GetMapping(value = "vaccineType/update/{String}")
    public String UpdateFormAddVaccineType(Model model, @PathVariable String id){
        model.addAttribute("vaccineType", vaccineTypeService.findById(id));
        return "vaccineTypeAdd";
    }
    @GetMapping(value = "vaccineType/update/{String}")
    public String DeleteFormVaccineType(Model model, @PathVariable String id){
        vaccineTypeService.delete(id);
        return "redirect:/VaccineTypeMain";
    }

}
