package com.example.employeeandvaccinemanagement.entities;

import jakarta.persistence.*;

@Entity
@Table
public class Vaccine {
    @Id
    private String vaccinId;
    private String contraindication;
    private String vaccinTypeId;
    @ManyToOne(targetEntity = VaccineType.class)
    @JoinColumn(name = "VACCINETYPE")
    private VaccineType vaccineType;
}
