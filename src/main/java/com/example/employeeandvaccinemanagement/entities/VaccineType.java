package com.example.employeeandvaccinemanagement.entities;

import jakarta.persistence.*;

@Entity
@Table
public class VaccineType {
    @Id
    private String vaccineTypeId;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "VACCINE_TYPE_NAME")
    private String vaccineTypeName;

    @Column(name ="STATUS")
    private Integer status;

    @Column(name = "Image")
    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public VaccineType(String vaccineTypeId, String description, String vaccineTypeName, Integer status,String image) {
        this.vaccineTypeId = vaccineTypeId;
        this.description = description;
        this.vaccineTypeName = vaccineTypeName;
        this.status = status;
        this.image = image;
    }

    public VaccineType() {
    }

    public String getVaccineTypeId() {
        return vaccineTypeId;
    }

    public void setVaccineTypeId(String vaccineTypeId) {
        this.vaccineTypeId = vaccineTypeId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVaccineTypeName() {
        return vaccineTypeName;
    }

    public void setVaccineTypeName(String vaccineTypeName) {
        this.vaccineTypeName = vaccineTypeName;
    }

    @Override
    public String toString() {
        return "VaccineType{" +
                "vaccineTypeId='" + vaccineTypeId + '\'' +
                ", description='" + description + '\'' +
                ", vaccineTypeName='" + vaccineTypeName + '\'' + status +
                '}';
    }
}
