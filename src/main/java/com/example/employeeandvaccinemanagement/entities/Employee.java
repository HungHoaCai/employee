package com.example.employeeandvaccinemanagement.entities;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name= "employee",schema = "dbo")
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private Integer employeeId;
    @Column(name = "ADDRESS")
    private String address;
    @Column(name = "DATE_OF_BIRTH")
    private Date DateOfBirth;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "EMPLOYEE_NAME")
    private String employeeName;
    @Column(name = "GENDER")
    private Integer gender;
    @Column(name = "IMAGE")
    private String image;
    @Column(name = "PASSWORD")
    private String password;
    @Column(name = "PHONE")
    private Integer phone;
    @Column(name = "POSTISION")
    private String position;
    @Column(name = "USERNAME")
    private String userName;
    @Column(name = "WORKING_PLACE")
    private String workingPlace;

    public Employee(Integer employeeId, String address, Date dateOfBirth, String email, String employeeName, Integer gender, String image, String password, Integer phone, String position, String userName, String workingPlace) {
        this.employeeId = employeeId;
        this.address = address;
        DateOfBirth = dateOfBirth;
        this.email = email;
        this.employeeName = employeeName;
        this.gender = gender;
        this.image = image;
        this.password = password;
        this.phone = phone;
        this.position = position;
        this.userName = userName;
        this.workingPlace = workingPlace;
    }

    public Employee() {
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getWorkingPlace() {
        return workingPlace;
    }

    public void setWorkingPlace(String workingPlace) {
        this.workingPlace = workingPlace;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "employeeId=" + employeeId +
                ", address='" + address + '\'' +
                ", DateOfBirth=" + DateOfBirth +
                ", email='" + email + '\'' +
                ", employeeName='" + employeeName + '\'' +
                ", gender=" + gender +
                ", image='" + image + '\'' +
                ", password='" + password + '\'' +
                ", phone=" + phone +
                ", position='" + position + '\'' +
                ", userName='" + userName + '\'' +
                ", workingPlace='" + workingPlace + '\'' +
                '}';
    }
}
