package com.example.employeeandvaccinemanagement.repositorys;

import com.example.employeeandvaccinemanagement.entities.Employee;
import com.example.employeeandvaccinemanagement.entities.VaccineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VaccineTypeRepository extends JpaRepository<VaccineType,String> {
    List<VaccineType> findAll();
}
